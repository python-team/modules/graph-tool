// graph-tool -- a general graph modification and manipulation thingy
//
// Copyright (C) 2006-2025 Tiago de Paula Peixoto <tiago@skewed.de>
//
// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU Lesser General Public License as published by the Free
// Software Foundation; either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "graph.hh"
#include "graph_filtering.hh"
#include "graph_selectors.hh"
#include "graph_hits.hh"

#include <boost/python.hpp>

using namespace std;
using namespace boost;
using namespace graph_tool;

struct get_hits_dispatch
{
    template <class Graph, class VertexIndex, class WeightMap,
              class CentralityMap>
    void operator()(Graph& g, VertexIndex vertex_index, WeightMap w,
                    CentralityMap x, std::any ay, double epsilon,
                    size_t max_iter, long double& eig) const
    {
        try
        {
            typename CentralityMap::checked_t y = std::any_cast<typename CentralityMap::checked_t>(ay);
            get_hits()(g, vertex_index, w, x,
                       y.get_unchecked(num_vertices(g)), epsilon, max_iter,
                       eig);
        }
        catch (std::bad_any_cast&)
        {
            throw GraphException("x and y vertex properties must be of the same type.");
        }
    }
};


long double hits(GraphInterface& g, std::any w, std::any x, std::any y,
                 double epsilon, size_t max_iter)
{
    if (w.has_value() && !belongs(writable_edge_scalar_properties, w))
        throw ValueException("edge property must be writable");
    if (!belongs(vertex_floating_properties, x))
        throw ValueException("vertex property must be of floating point"
                             " value type");

    typedef UnityPropertyMap<int,GraphInterface::edge_t> weight_map_t;
    auto weight_props = hana::append(writable_edge_scalar_properties,
                                    hana::type<weight_map_t>());

    if (!w.has_value())
        w = weight_map_t();

    long double eig = 0;
    run_action<>()
        (g,
         [&](auto&& graph, auto&& a2, auto&& a3)
         {
             return get_hits_dispatch()
                 (std::forward<decltype(graph)>(graph), g.get_vertex_index(),
                  std::forward<decltype(a2)>(a2),
                  std::forward<decltype(a3)>(a3), y, epsilon, max_iter, eig);
         },
         weight_props, vertex_floating_properties)(w, x);
    return eig;
}

#define __MOD__ centrality
#include "module_registry.hh"
REGISTER_MOD
([]
 {
     using namespace boost::python;
     def("get_hits", &hits);
 });
