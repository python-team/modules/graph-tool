Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: graph-tool
Upstream-Contact: Tiago de Paula Peixoto <tiago@skewed.de>
Source: https://graph-tool.skewed.de
Files-Excluded:
 m4/ax_boost_base.m4
 m4/ax_boost_coroutine.m4
 m4/ax_boost_iostreams.m4
 m4/ax_boost_python.m4
 m4/ax_boost_regex.m4
 m4/ax_boost_thread.m4
 m4/ax_create_pkgconfig_info.m4
 m4/ax_cxx_compile_stdcxx_17.m4
 m4/ax_cxx_compile_stdcxx.m4
 m4/ax_openmp.m4
 m4/ax_python_devel.m4
 m4/ax_python_module.m4
 aclocal.m4
 config.h.in
 */Makefile.in
 build-aux
 config.h
 configure

Files: *
Copyright:
 2006-2025 Tiago de Paula Peixoto <tiago@skewed.de>
License: LGPL-3+ and GPL-3+

Files: src/graph/graph_reverse.hh
Copyright:
 2006-2025 Tiago de Paula Peixoto <tiago@skewed.de>
 2000 David Abrahams
License: BSL-1.0 or LGPL-3+

Files: src/graph/graph_filtered.hh
Copyright:
 2006-2025 Tiago de Paula Peixoto <tiago@skewed.de>
 1997-2000 University of Notre Dame
License: BSL-1.0 or LGPL-3+

Files: src/graph/fast_vector_property_map.hh
Copyright:
 2006-2025 Tiago de Paula Peixoto <tiago@skewed.de>
 2003 Vladimir Prus
License: BSL-1.0 or LGPL-3+

Files: src/graph/inference/support/spence.cc
Copyright:
 2006-2025 Tiago de Paula Peixoto <tiago@skewed.de>
 1985,1987,1989 Stephen L. Moshier
License: LGPL-3+

Files:
 src/pcg-cpp/include/pcg_extras.hpp
 src/pcg-cpp/include/pcg_random.hpp
 src/pcg-cpp/include/pcg_uint128.hpp
Copyright:
 2014-2019 Melissa O'Neill <oneill@pcg-random.org>
License: Apache-2.0

Files:
 src/boost-workaround/boost/graph/graphml.hpp
 src/graph/graphml.cpp
Copyright:
 2006 Tiago de Paula Peixoto <tiago@skewed.de>
 2004 The Trustees of Indiana University
License: BSL-1.0

Files:
 src/boost-workaround/boost/graph/adjacency_iterator.hpp
 src/boost-workaround/boost/graph/betweenness_centrality.hpp
 src/boost-workaround/boost/graph/detail/read_graphviz_new.hpp
 src/boost-workaround/boost/graph/kamada_kawai_spring_layout.hpp
 src/boost-workaround/boost/graph/overloading.hpp
 src/graph/read_graphviz_new.cpp
Copyright:
 2004 The Trustees of Indiana University
License: BSL-1.0

Files:
 src/boost-workaround/boost/graph/copy_alt.hpp
 src/boost-workaround/boost/graph/named_function_params-alt.hpp
 src/boost-workaround/boost/graph/push_relabel_max_flow.hpp
Copyright:
 1997-2001 University of Notre Dame
License: BSL-1.0

Files:
 src/boost-workaround/boost/graph/metric_tsp_approx.hpp
Copyright:
 2008 Matyas W Egyhazy
License: BSL-1.0

Files:
 src/boost-workaround/boost/graph/stoer_wagner_min_cut.hpp
Copyright:
 2010 Daniel Trebbien
License: BSL-1.0

Files:
 src/boost-workaround/boost/graph/vf2_sub_graph_iso.hpp
Copyright:
 2013 Jakob Lykke Andersen, University of Southern Denmark
 2012 Flavio De Lorenzi <fdlorenzi@gmail.com>
License: BSL-1.0

Files: src/boost-workaround/boost/graph/graphviz.hpp
Copyright:
 2003 Jeremy Siek
 2001 University of Notre Dame.
License: BSL-1.0

Files:
 src/boost-workaround/boost/graph/maximum_weighted_matching.hpp
Copyright:
 2018 Yi Ji
License: BSL-1.0

Files: m4/ax_boost_context.m4
Copyright:
 2008 Michael Tindal
 2008 Thomas Porschberg <thomas@randspringer.de>
 2013 Daniel Casimiro <dan.casimiro@gmail.com>
License: FSFAP

Files: m4/ax_lib_cgal_core.m4
Copyright:
 2010 Sebastian Hegler <sebastian.hegler@tu-dresden.de>
License: FSFAP

Files: m4/ax_boost_graph.m4
Copyright:
 2008 Thomas Porschberg <thomas@randspringer.de>
License: FSFAP

Files:
 src/graph_tool/collection/*.gt.gz
Copyright:
 2006-2025 Tiago de Paula Peixoto <tiago@skewed.de>
License: public-domain-gt
 These files contain scientific data that are popular in network science
 and as such in the public domain. `src/graph_tool/collection/__init__.py`
 provides details about their origin.

Files: debian/*
Copyright:
 2022-2025 Jerome Benoit <calculus@debian.org>
License: GPL-3+

Files: debian/adhoc/*
Copyright:
 2006-2025 Tiago de Paula Peixoto
License: public-domain-gt-adhoc
 This material is literaly in the public domain at the below address.
Comment:
 Retrieved as-is by hand from https://graph-tool.skewed.de

Files:
 debian/adhoc/provide/doc/extlinks_fancy.py
Copyright:
 2007-2017 Sphinx team
License: BSD-2-clause

License: GPL-3+
 This package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-3+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

License: BSL-1.0
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY AUTHORS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.
