.. _submodules:

Submodules
==========

``graph-tool`` is subdivided into the following submodules:

.. toctree::
  :titlesonly:

  graph_tool
  centrality
  clustering
  collection
  correlations
  dynamics
  draw
  flow
  generation
  inference
  search_module
  spectral
  stats
  topology
  util
